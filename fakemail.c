#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>

/*
 * We need to use the cs server because it allows relaying as long as the client
 * is on the Sierra College LAN.
 */

void error(char *);

int main(int argc, char **argv)
{
    struct sockaddr_in sa;
    int sockfd;

    if (argc < 3)
    {
        fprintf(stderr, "Usage: %s recipient \"message\"\n", argv[0]);
	    exit(1);
    }

    // Look up IP address
    struct hostent *he = gethostbyname("cs.sierracollege.edu");
    char *ip = he->h_addr_list[0];
    
    sa.sin_family = AF_INET;
    sa.sin_port = htons(25);
    sa.sin_addr = *((struct in_addr *)ip);
    
    sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sockfd == -1) {
        fprintf(stderr, "Can't create socket\n");
	exit(3);
    }
    int res = connect(sockfd, (struct sockaddr *)&sa, sizeof(sa));
    if (res == -1) {
        fprintf(stderr, "Can't connect\n");
	exit(2);
    }

    char buf[1000];
    size_t rsize;
    int resultcode;

    // Get greeting from server. Expecting 220
    rsize = recv(sockfd, buf, 1000, 0);
    sscanf(buf, "%d", &resultcode);
    if (resultcode != 220) error("Didn't get 220");

    // HELO cs.sierracollege.edu
    sprintf(buf, "HELO cs.sierracollege.edu\n");
    send(sockfd, buf, strlen(buf), 0);

    // Expecting 250
    rsize = recv(sockfd, buf, 1000, 0);
    sscanf(buf, "%d", &resultcode);
    if (resultcode != 250) error("Didn't get 250 for HELO");

    // MAIL From:
    sprintf(buf, "MAIL From:<santa@northpole.com>\n");
    send(sockfd, buf, strlen(buf), 0);

    // Expecting 250
    rsize = recv(sockfd, buf, 1000, 0);
    sscanf(buf, "%d", &resultcode);
    if (resultcode != 250) error("Didn't get 250 for MAIL");

    // RCPT To:
    // We get this from the command line
    sprintf(buf, "RCPT To:<%s>\n", argv[1]);
    send(sockfd, buf, strlen(buf), 0);

    // Expecting 250
    rsize = recv(sockfd, buf, 1000, 0);
    sscanf(buf, "%d", &resultcode);
    if (resultcode != 250) error("Didn't get 250 for RCPT");

    // DATA
    sprintf(buf, "DATA\n");
    send(sockfd, buf, strlen(buf), 0);

    // Expecting 354
    rsize = recv(sockfd, buf, 1000, 0);
    sscanf(buf, "%d", &resultcode);
    if (resultcode != 354) error("Didn't get 354 for DATA");

    // Send body
    // From: fake
    // To: fake
    // Subject: whatever
    //
    // Body (from command line)
    // .
    sprintf(buf, "From: joe@northpole.com\n");
    send(sockfd, buf, strlen(buf), 0);
    sprintf(buf, "To: noone@example.com\n");
    send(sockfd, buf, strlen(buf), 0);
    sprintf(buf, "Subject: ha ha\n");
    send(sockfd, buf, strlen(buf), 0);
    sprintf(buf, "\n");
    send(sockfd, buf, strlen(buf), 0);
    sprintf(buf, "%s\n", argv[2]);
    send(sockfd, buf, strlen(buf), 0);
    sprintf(buf, ".\n");
    send(sockfd, buf, strlen(buf), 0);

    // Expecting 250
    rsize = recv(sockfd, buf, 1000, 0);
    sscanf(buf, "%d", &resultcode);
    if (resultcode != 250) error("Didn't get 250 for end of DATA");

    // QUIT
    sprintf(buf, "QUIT\n");
    send(sockfd, buf, strlen(buf), 0);

    // Expecting 221
    rsize = recv(sockfd, buf, 1000, 0);
    sscanf(buf, "%d", &resultcode);
    if (resultcode != 221) error("Didn't get 221 for QUIT");

    close(sockfd);

}

void error(char *s)
{
    fprintf(stderr, "%s\n", s);
    exit(1);
}