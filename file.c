#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>

FILE* connecc();
void menu();
void makelist(FILE *s);
char get_choice();
void list_files(FILE *s);
void download(FILE *s);
void quit(FILE *s);
void all(FILE *s);

struct File
{
    int size;
    char name[];
};

int main()
{
    // connect is already defined in stdio.h or something like that. Had to rename
    FILE * s = connecc();

    
    // Receive greeting
    char buf[1000];
    fgets(buf, 1000, s);
    if (strncmp(buf, "+OK", 3) != 0)
    {
        printf("Didn't get greeting\n");
        exit(1);
    }
    
    // makelist(s);
    menu();
    
    do
    {
        // Get choice
        char choice = get_choice();
        
        // Handle choice
        switch(choice)
        {
            case 'l':
            case 'L':
                list_files(s);
                break;
            
            case 'd':
            case 'D':
                download(s);
                break;
                
            case 'q':
            case 'Q':
                quit(s);
                exit(1);
                break;
                
            case 'a':
            case 'A':
                all(s);
                printf("Downloading all files\n");
                break;
                
            default:
                printf("Choice must be d, l, or q\n");
        }
    }
    while(1);
    
}

FILE* connecc()
{
    struct hostent *he = gethostbyname("runwire.com");
    struct in_addr *ip = (struct in_addr*)he->h_addr_list[0];
    struct sockaddr_in sa;
    sa.sin_family = AF_INET;
    sa.sin_port = htons(1234);
    sa.sin_addr = *ip;
    
    int sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sockfd == -1)
    {
        printf("Can't create socket\n");
        exit(1);
    }
    
    int res = connect(sockfd, (struct sockaddr *)&sa, sizeof(sa));
    if (res == -1)
    {
        printf("Can't connecc to server\n");
        exit(2);
    }
    
    return (fdopen(sockfd, "r+"));
}

void menu()
{
    printf("L) List files\n");
    printf("D) Download a file\n");
    printf("A) Download all files\n");
    printf("Q) Quit\n");
    printf("\n");
}

void makelist(FILE *s)
{
    fprintf(s, "LIST");
    do
    {
        char buf[1000];
        fgets(buf, 1000, s);
        if (strcmp(buf, "+OK\n") == 0) continue;
        if (strcmp(buf, ".\n") == 0) break;
        
    } while (1);
}

char get_choice()
{
    printf("Your choice: ");
    char buf[100];
    fgets(buf, 100, stdin);
    return buf[0];
}

void list_files(FILE *s)
{
    char buf[100];

    printf("\n");
    do
    {
        fgets(buf, 1000, s);
        if (strcmp(buf, "+OK\n") == 0) continue;
        if (strcmp(buf, ".\n") == 0) break;
        printf("%s", buf);
    } while (1);
    printf("\n");
}

void download(FILE *s)
{
    int size;
    int recieved;
    char buf[1000];
    char filename[1000];
    printf("What file?: ");
    fgets(filename, 1000, stdin);
    printf("%s ", filename);
    
    // size
    fprintf(s, "SIZE %s", filename);
    fgets(buf, sizeof("+OK "), s);
    fgets(buf, 1000, s);
    sscanf(buf, "%d", &size);
    printf("%d\n", size);

    // open output 
    FILE* outptr = fopen(filename, "w");
    if (outptr == NULL)
    {
        fprintf(stderr, "Could not create %s.\n", filename);
    }
    
    fprintf(s, "GET %s", filename);
    char buffer[sizeof("+OK\n")];
    fgets(buffer,sizeof("+OK\n") , s);
    
    while (size > 0)
    {
        size_t insize = size > 1000 ? 1000 : size;
        printf("size: %d\n", size);
        fread(buf, 1, insize, s);
        printf("insize: %zu\n", insize);
        fwrite(buf, insize, 1, outptr);
        size -= insize;
    }
    
    // close outfile
    printf("\nContents copied to %s", filename);
    fclose(outptr);
}

void quit(FILE *s)
{
    fprintf(s, "QUIT");
    fclose(s);
}

void all(FILE *s)
{
    // loop through downloading all the bad boys
}