#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>

int main()
{
    // Connect to server
    struct hostent *he = gethostbyname("runwire.com");
    struct in_addr *ip = (struct in_addr*)he->h_addr_list[0];
    struct sockaddr_in sa;
    sa.sin_family = AF_INET;
    sa.sin_port = htons(1234);
    sa.sin_addr = *ip;
    
    int sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sockfd == -1)
    {
        printf("Can't create socket\n");
        exit(1);
    }
    
    int res = connect(sockfd, (struct sockaddr *)&sa, sizeof(sa));
    if (res == -1)
    {
        printf("Can't connect to server\n");
        exit(2);
    }
    
    FILE *s = fdopen(sockfd, "r+");
    
    // Receive greeting
    char buf[1000];
    fgets(buf, 1000, s);
    if (strncmp(buf, "+OK", 3) != 0)
    {
        printf("Didn't get greeting\n");
        exit(1);
    }
    
    // Issue LIST commant
    fprintf(s, "LIST\n");
    
    // Receive file listing 
    do
    {
        fgets(buf, 1000, s);
        if (strcmp(buf, "+OK\n") == 0) continue;
        if (strcmp(buf, ".\n") == 0) break;
        printf("%s", buf);
    } while (1);
    
    // Close connection
    fprintf(s, "QUIT\n");
    
    close(sockfd);
}