#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>

void connecc();
void menu();
char get_choice();
void list_files(FILE *s);
void download();
void quit(FILE *s);
void all(FILE *s);

int main()
{
 do
    {
            
        // Menu
        menu();
        
        // Get choice
        char choice = get_choice();
        
        // Handle choice
        switch(choice)
        {
            case 'l':
            case 'L':
                break;
            
            case 'd':
            case 'D':
                download();
                break;
                
            case 'q':
            case 'Q':
                exit(1);
                break;
                
            case 'a':
            case 'A':
                printf("Downloading all files\n");
                break;
                
            default:
                printf("Choice must be d, l, or q\n");
        }
    }
    while(1);
    
}

void menu()
{
    printf("\n");
    printf("L) List files\n");
    printf("D) Download a file\n");
    printf("A) Download all files\n");
    printf("Q) Quit\n");
    printf("\n");
}

char get_choice()
{
    printf("Your choice: ");
    char buf[100];
    fgets(buf, 100, stdin);
    printf("\n");
    return buf[0];
}

void download()
{
    printf("Filename: ");
    char buf[100];
    fgets(buf, 100, stdin);
    printf("\n");
    printf("GET %c\n", get_choice());
}