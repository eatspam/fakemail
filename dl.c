#include <stdio.h>

FILE * connect();
void menu();
char get_choice();
void list_files(FILE *s);
void download(FILE *s);
void quit(FILE *s);

// host nc runwire 1234
// http://runwire.com/~barry/server_status.html

int main()
{
    // connect
    FILE * s = connect();
    
    // menu
    menu();
    
    // get choice
    char choice = get_choice();
    // handle choice
    switch(choice)
    {
        case 'l':
        case 'L':
            list_files(s);
            break;
            
        case 'd':
        case 'D':
            download(s);
            break;
            
        case 'q':
        case 'Q':
            quit(s);
            exit(0);
            
        case 'a':
        case 'A':
            printf("download all of them\n");
            break;
            
        default:
            printf("Valid inputs: l, d, q\n");
    }
    
    // dc
}

FILE * connect()
{
    return NULL;
}

void menu()
{
    printf("List files\n");
    printf("Download a file\n");
    printf("Quit\n");
    printf("\n");
}

char get_choice()
{
    printf("Your choice: ");
    char buf[100];
    fgets(buf, 100, stdin);
}

void list_files(FILE *s)
{
    
}

void download(FILE * s)
{
    
}

void quit(FILE *s)
{
    quit(s);
}